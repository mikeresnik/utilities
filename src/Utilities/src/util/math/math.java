package util.math;

public final class math {

    private math() {
    }
    
    public static double sign(double d){
        return Double.compare(d, 0);
    }
    
}
