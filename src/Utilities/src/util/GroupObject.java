package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import javafx.util.Pair;
import util.math.functions.Function2;

public class GroupObject<T> implements Collection<T> {

    private List<T> elements;
    private T identifiableElement;
    private Pair<Boolean, Comparator<T>> maintainOrderPair;
    private Function2<T, T, Boolean> equalsMethod;
    private Function2<T, T, Boolean> previousEquals;
    private Function<GroupObject<T>, String> toString;

    public GroupObject(T identifiableElement) {
        this.elements = new ArrayList();
        this.identifiableElement = identifiableElement;
        if (identifiableElement != null) {
            this.elements.add(identifiableElement);
        }
        this.maintainOrderPair = null;
        this.setDefaultEquals();
        this.setDefaultToString();
    }

    public void setDefaultEquals() {
        this.equalsMethod = (T t1, T t2) -> (t1 == t2) || (t1 != null && t2 != null && t1.equals(t2));
    }

    public void setDefaultToString() {
        Function<GroupObject<T>, String> stringy = (GroupObject<T> gr) -> (gr.identifiableElement + " : " + gr.elements);
        this.toString = stringy;
    }

    public void setEqualsMethod(Function2<T, T, Boolean> equalsMethod) {
        this.previousEquals = this.equalsMethod;
        this.equalsMethod = equalsMethod;
    }

    public T getIdentifiableElement() {
        return this.identifiableElement;
    }

    public GroupObject<T> setIdentifiableElement(T identifiableElement) {
        GroupObject<T> retGroup = new GroupObject(identifiableElement);
        retGroup.setEqualsMethod(this.equalsMethod);
        retGroup.setMaintainOrderPair(this.maintainOrderPair);
        retGroup.addAll(this.elements);
        return retGroup;
    }

    private void maintainOrder() {
        if (this.maintainOrderPair == null || this.maintainOrderPair.getKey() == false || this.maintainOrderPair.getValue() == null) {
            return;
        }
        this.elements.sort(this.maintainOrderPair.getValue());
    }

    public Pair<Boolean, Comparator<T>> getMaintainOrderPair() {
        return maintainOrderPair;
    }

    public void setMaintainOrderPair(Pair<Boolean, Comparator<T>> maintainOrderPair) {
        this.maintainOrderPair = maintainOrderPair;
    }

    @Override
    public int size() {
        return this.elements.size();
    }

    @Override
    public boolean isEmpty() {
        return this.elements.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return this.elements.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return this.elements.iterator();
    }

    @Override
    public Object[] toArray() {
        return this.elements.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return this.elements.toArray(ts);
    }

    @Override
    public boolean add(T e) {
        if (this.equalsMethod.apply(e, this.getIdentifiableElement()) && this.contains(e) == false) {
            boolean ret = this.elements.add(e);
            this.maintainOrder();
            return ret;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        boolean ret = this.elements.remove(o);
        this.maintainOrder();
        return ret;
    }

    @Override
    public boolean containsAll(Collection<?> clctn) {
        Iterator<?> iter = clctn.iterator();
        while (iter.hasNext()) {
            Object curr = iter.next();
            if (this.contains(curr) == false) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> clctn) {
        Iterator<?> iter = clctn.iterator();
        boolean retBool = true;
        while (iter.hasNext()) {
            T curr = (T) iter.next();
            retBool &= this.add(curr);
        }
        return retBool;
    }

    @Override
    public boolean removeAll(Collection<?> clctn) {
        Iterator<?> iter = clctn.iterator();
        boolean retBool = true;
        while (iter.hasNext()) {
            Object curr = iter.next();
            retBool &= this.remove(curr);
        }
        return retBool;
    }

    @Override
    public boolean retainAll(Collection<?> clctn) {
        List<T> toRemove = new ArrayList();
        for (int i = 0; i < this.elements.size(); i++) {
            T curr = this.elements.get(i);
            if (clctn.contains(curr) == false) {
                toRemove.add(curr);
            }
        }
        return this.elements.removeAll(toRemove);
    }

    @Override
    public void clear() {
        this.elements.clear();
    }

    @Override
    public String toString() {
        return this.toString.apply(this);
    }

    public void setNonNullCondition(Function2<T, T, Boolean> function) {
        this.previousEquals = this.equalsMethod;
        Function2<T, T, Boolean> tempMethod = new Function2<T, T, Boolean>() {
            @Override
            public Boolean apply(T t1, T t2) {
                boolean functionResult = function.apply(t1, t2);
                boolean retValue = (t1 == t2) || (t1 != null && t2 != null && functionResult);
                return retValue;
            }
        };
        this.setEqualsMethod(tempMethod);
    }

    public void behaveLikeNamedList(boolean condition) {
        if (condition) {
            this.setEqualsMethod((T t1, T t2) -> (true));
        } else {
            // Backcycle to previous settings
            if (this.previousEquals == null) {
                System.out.println("PREVIOUS EQUALS WAS NULL");
                this.setDefaultEquals();
            } else {
                this.equalsMethod = this.previousEquals;
            }
        }
    }

    public void setToString(Function<GroupObject<T>, String> toString) {
        this.toString = toString;
    }

    public List<T> getElements() {
        return elements;
    }

    public Function2<T, T, Boolean> getEqualsMethod() {
        return equalsMethod;
    }

}
