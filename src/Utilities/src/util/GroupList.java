package util;

import java.util.function.Function;
import util.math.functions.Function2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mike
 */
public class GroupList<T> extends GroupObject<GroupObject<T>>{
    
    private Function2<T,T,Boolean> subEqualsMethod;
    private Function<GroupObject<T>, String> subToString;
    
    public GroupList(GroupObject<T> identifiableElement) {
        super(identifiableElement);
        this.behaveLikeNamedList(true);
        this.setSubEqualsMethod(
                (T t1, T t2) -> (false)
        );
        this.setSubToString(
                (GroupObject<T> gr) -> ("")
        );
    }
    
    @Override
    public boolean add(GroupObject<T> element){
        boolean superRes = super.add(element);
        if(superRes){
            element.setEqualsMethod(subEqualsMethod);
            element.setToString(subToString);
        }
        return superRes;
    }
    
    public boolean addElement(T element){
        for(GroupObject<T> elem : this.getElements()){
            if(elem == null){
                continue;
            }
            if(subEqualsMethod.apply(elem.getIdentifiableElement(), element)){
                System.out.println("MATCH AT ELEM:" + elem);
                return elem.add(element);
            }
        }
        return add(new GroupObject(element));
    }
    
    public boolean addElements(T ... elements){
        boolean retValue = true;
        for(T elem : elements){
            retValue &= addElement(elem);
        }
        return retValue;
    }
    
    public void setSubEqualsMethod(Function2<T, T, Boolean> equals){
        for(GroupObject<T> elem : this.getElements()){
            elem.setEqualsMethod(equals);
        }
        this.subEqualsMethod = equals;
    }
    
    public void setSubToString(Function<GroupObject<T>, String> toString){
        for(GroupObject<T> elem : this.getElements()){
            elem.setToString(toString);
        }
        this.subToString = toString;
    }
    
    public GroupObject<T> getGroupMatching(T element){
        for(GroupObject<T> elem : this.getElements()){
            if(elem == null){
                continue;
            }
            if(subEqualsMethod.apply(elem.getIdentifiableElement(), element)){
                return elem;
            }
        }
        return null;
    }
    
    public GroupList(){
        this(null);
    }
    
}
