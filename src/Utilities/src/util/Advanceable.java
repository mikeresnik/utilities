package util;


public interface Advanceable {

    public void advance();
    
}
